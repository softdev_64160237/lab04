/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.lab04;

/**
 *
 * @author EliteCorps
 */
public class gameBoard {

    char[][] boardChar;

    public gameBoard() {
        boardChar = new char[3][3];
        initializeBoard();
    }

    private void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                boardChar[i][j] = '-';
            }
        }
    }

    public void updateBoard(int move, char symbol) {
        int pRow = (move - 1) / 3;
        int pCol = (move - 1) % 3;
        boardChar[pRow][pCol] = symbol;
    }

    public boolean checkValidMove(int move) {
        int pRow = (move - 1) / 3;
        int pCol = (move - 1) % 3;
        if (move < 1 || move > 9) {
            System.out.println("Invalid move, please input number between 1-9");
            return false;
        }
        
        if (boardChar[pRow][pCol] != '-') {
            System.out.println("Invalid move, Position already taken");
            return false;
        }
        return true;
    }
    
    public boolean checkWin() {
        return checkRow() || checkCol() || checkDiagnol();
    }

    public boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (boardChar[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (boardChar[i][0] != '-' && boardChar[i][0] == boardChar[i][1] && boardChar[i][0] == boardChar[i][2]) {
                return true;
            }
        }
        return false;
    }

    private boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (boardChar[0][i] != '-' && boardChar[0][i] == boardChar[1][i] && boardChar[0][i] == boardChar[2][i]) {
                return true;
            }
        }
        return false;
    }

    private boolean checkDiagnol() {
        return (boardChar[0][0] != '-' && boardChar[0][0] == boardChar[1][1] && boardChar[0][0] == boardChar[2][2])
                || (boardChar[0][2] != '-' && boardChar[0][2] == boardChar[1][1] && boardChar[0][2] == boardChar[2][0]);
    }
}
