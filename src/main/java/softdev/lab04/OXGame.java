/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package softdev.lab04;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author EliteCorps
 */
public class OXGame {

    private boolean playAgain;
    private gameBoard board;
    private Player player1, player2, currentPlayer;

    public OXGame() {
        playAgain = true;
        board = new gameBoard();
        player1 = new Player('X');
        player2 = new Player('O');
        currentPlayer = player1;
    }

    public void playGame() {
        printWelcome();
        while (playAgain) {
            board = new gameBoard();
            currentPlayer = player1;
            while (true) {
                printBoard();
                printTurn();
                inputMove();
                if (board.checkWin()) {
                    printBoard();
                    switchTurn();
                    printWinner();
                    printScore();
                    break;
                }
                if (board.checkDraw()) {
                    printBoard();
                    printDraw();
                    printScore();
                    break;
                }
            }
            resetGame();
        }
        printThankForGame();
    }

    private void printWelcome() {
        System.out.println("<-- Welcome to OX Game!! -->");
    }

    private void printThankForGame() {
        System.out.println("<-- Thanks for playing OX Game!! -->");
    }

    private void printBoard() {
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board.boardChar[i][j] + " | ");
            }
            System.out.println("");
            System.out.println("-------------");
        }
    }

    private void printTurn() {
        System.out.println("Current turn: " + currentPlayer.getSymbol());
    }

    private void inputMove() {
        if (currentPlayer == player1) {
            Scanner sc = new Scanner(System.in);
            System.out.print("Please enter your move (1-9): ");
            int move = sc.nextInt();
            if (board.checkValidMove(move)) {
                board.updateBoard(move, currentPlayer.getSymbol());
                switchTurn();
            }
        } else {
            Random bot = new Random();
            int move;
            do {
                move = bot.nextInt(9) + 1;
            } while (!board.checkValidMove(move));
            board.updateBoard(move, currentPlayer.getSymbol());
            switchTurn();
        }
    }

    private void switchTurn() {
        if (currentPlayer == player1) {
            currentPlayer = player2;
        } else {
            currentPlayer = player1;
        }
    }

    private void printWinner() {
        Player winner = currentPlayer;
        Player loser;
        if (currentPlayer == player1) {
            loser = player2;
        } else {
            loser = player1;
        }
        System.out.println("Game Over!! " + winner.getSymbol() + "'s is the winner!!");
        winner.win();
        loser.lose();
    }

    private void printDraw() {
        System.out.println("Game Over!! It's a draw!!");
        player1.draw();
        player2.draw();
        
    }

    private void resetGame() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to play again? (Y/N): ");
        String reset = sc.next().trim().toLowerCase();
        playAgain = reset.equals("y");
    }
    
    private void printScore()  {
        System.out.println("Player X - Win: " + player1.getWinCount() + " | Lose: " + player1.getLoseCount() + " | Draw: " + player1.getDrawCount());
        System.out.println("Player O - Win: " + player2.getWinCount() + " | Lose: " + player2.getLoseCount() + " | Draw: " + player2.getDrawCount());
    }
}
